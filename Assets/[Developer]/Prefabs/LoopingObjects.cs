using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class LoopingObjects : MonoBehaviour
{
    public bool moveAlongX;
    public bool moveAlongY;

    public float moveDistance;
    public float moveDuration;

    private Vector3 startPosition;

    private void OnEnable()
    {            
        startPosition = transform.position;
        StartMoving();
    }

    void StartMoving()
    {
        if (moveAlongX)
        {
            transform.DOMoveX(startPosition.x + moveDistance, moveDuration).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
        }

        if (moveAlongY)
        {
            transform.DOMoveY(startPosition.y + moveDistance, moveDuration).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
        }
    }

    void Update()
    {
        if (moveAlongX && transform.position.x > Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0)).x)
        {
            transform.position = startPosition;
        }

        if (moveAlongY && transform.position.y > Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0)).y)
        {
            transform.position = startPosition;
        }
    }
    private void OnDisable()
    {
        transform.position = startPosition;
    }
}
