using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class Player : MonoBehaviour
{
    public static Player instance;

    public VariableJoystick fixedJoystick;
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] float forceAmount;
    public Tile currentTile;
    public bool isPlayerDead;
    public Rigidbody2D rb;

    public bool canBreakTile = true;
    public bool iscolliding;
    public int score= 0;
    public bool canCheckOutOfBounds = true;
    void Start()
    {
        if (TryGetComponent(out AIPlayer p) && p != null)
            currentTile = TileGenerator.tilesArray[1, 8];
        else
            currentTile = TileGenerator.tilesArray[4, 1];

        score = 0;
        if (instance == null)
            instance = this;
        rb = GetComponent<Rigidbody2D>();

        isPlayerDead = false;
        iscolliding = true;


    }

    private void OnEnable()
    {
        Debug.Log($"vsComputer {TileGenerator.instance.vsComputer}");
        if (TileGenerator.instance.vsComputer && TryGetComponent(out AIPlayer p))
        {
                p.enabled = true;
            fixedJoystick.gameObject.SetActive(false);            
        }
        else
            fixedJoystick.gameObject.SetActive(true);

        //currentTile = TileGenerator.instance.GetTile((int)transform.position.x, (int)transform.position.y);
        TileGenerator.roundCleanUp += CleanMess;
        //transform.GetComponent<BoxCollider2D>().enabled = false;
        Time.timeScale = 1;
        canBreakTile = true;
        canCheckOutOfBounds = true;
    }

    private void OnDisable()
    {
        TileGenerator.roundCleanUp -= CleanMess;
    }

    private void CleanMess()
    {
        //clean up after round complete
        currentTile = null;
        iscolliding = false;

        foreach (Tile t in TileGenerator.tilesArray)
        {
            t.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            t.isBroken = false;
            t.timeOnTile = 0;
            t.timerRunning = false;
        }
    }

    void FixedUpdate()
    {

        ///if (TileGenerator.instance.vsComputer && transform.name == "Player - 2")
           // return;

        if (TileGenerator.isGameComplete || TileGenerator.roundComplete)
        {
            rb.velocity = Vector2.zero; 
            return;
        }

        //if(transform.position.x <= 0.3f || transform.position.x >= 5.3f || transform.position.y <= 0.3f || transform.position.y >= 9.3f && canCheckOutOfBounds)
        //if(CheckApproximity(transform.position.x,-0.4f) || CheckApproximity(transform.position.x, 5.4f) || CheckApproximity(transform.position.y, -0.4f)
          //  || CheckApproximity(transform.position.y, 9.4f) && canCheckOutOfBounds)
        {
            //canCheckOutOfBounds = false;
            Debug.Log("PlayerOutOfBounds");
          //  CheckRoundWinner();

        }

        float horizontalInput = fixedJoystick.Horizontal;
        float verticalInput = fixedJoystick.Vertical;
        Vector3 movementDirection = new Vector2(horizontalInput, verticalInput).normalized;
        if (name == "Player - 1")
            rb.velocity = moveSpeed * Time.deltaTime * movementDirection;
        else if (!TileGenerator.instance.vsComputer)
            rb.velocity = moveSpeed * Time.deltaTime * movementDirection;

    }

    bool CheckApproximity(float value1, float value2)
    {
        return Mathf.Abs(value2 - value1) < 0.02;
    }


    public void CheckRoundWinner()
    {
        if (canCheckOutOfBounds)
            return;

        bool hasFirstPScored;
        if (name == "Player - 2")
        {
            TileGenerator.instance.player1.score++;
            Debug.Log("player1.score" + TileGenerator.instance.player1.score);
            hasFirstPScored = true;
        }
        else
        {
                TileGenerator.instance.player2.score++;
            Debug.Log("player1.score" + TileGenerator.instance.player2.score);
            hasFirstPScored = false;            
        }

        foreach (Tile t in TileGenerator.tilesArray)
        {
            t.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            t.isBroken = false;
            t.timeOnTile = 0;
            t.timerRunning = false;
        }

        TileGenerator.instance.RoundComplete(hasFirstPScored);

        return;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Tile") && !collision.gameObject.GetComponent<Tile>().isBroken)
        {
            iscolliding = true;
        }

        Tile t = collision.gameObject.GetComponent<Tile>();
        currentTile = TileGenerator.instance.GetTile(t.xPos, t.yPos);
        t.tileParticles.Play();
        // Debug.Log($"CurrentTile : {currentTile}\t X: {currentTile.xPos} Y: {currentTile.yPos} \tPlayer: {transform.name}");
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{

    //    if (collision.gameObject != this.gameObject)
    //    {
    //        Vector2 collisionDirection = collision.transform.position - transform.position;
    //        collisionDirection.Normalize();

    //        //if (collision.gameObject.TryGetComponent(out AIPlayer p) && p != null)
    //        {
    //            //Debug.Log("collided with AI-----------");

    //            if (!TileGenerator.instance.vsComputer)
    //            {
    //                //     transform.DOScale(new Vector2(0.5f,0.5f), 0.5f).OnComplete(() => AIPlayer.continueCoroutine = true);
    //                //if(collision.rigidbody.velocity.x< 0.3f || collision.rigidbody.velocity.y < 0.3)
    //                if(collision.rigidbody.velocity.magnitude < collision.otherRigidbody.velocity.magnitude)
    //                {
    //                    //AIPlayer.continueCoroutine = false;
    //                Debug.Log($"{name} \t   force added============DirectionX: {AIPlayer.directionX} \t DirectionY: {AIPlayer.directionY} \t {collision.otherRigidbody} \t {collision.rigidbody}");
    //                    //collision.rigidbody.AddForce(forceAmount * Time.deltaTime * collisionDirection, ForceMode2D.Force);   
    //                    //StartCoroutine(ApplyContinuousForce(collision.rigidbody, forceAmount, collisionDirection, 0.3f));
    //                    //collision.rigidbody.velocity += forceAmount * -collisionDirection;
    //                }
    //                //else if (collision.otherRigidbody.velocity.x > 0.3f || collision.otherRigidbody.velocity.y > 0.3f)
    //                {
    //                    //Debug.Log($"{name} \t   force added============DirectionX: {AIPlayer.directionX} \t DirectionY: {AIPlayer.directionY} \t {collision.otherRigidbody} \t {collision.rigidbody}");
    //                    //collision.otherRigidbody.AddForce(forceAmount * Time.deltaTime * collisionDirection, ForceMode2D.Force);
    //                }

                    
    //                //collision.rigidbody.AddForce(forceAmount * Time.deltaTime * -collisionDirection, ForceMode2D.Impulse);
    //            //AIPlayer.directionX *= -collisionDirection.x;
    //            //AIPlayer.directionY *= -collisionDirection.y;
    //              //  Debug.Log($"At 2nd outer border DirectionX: {AIPlayer.directionX} \t DirectionY: {AIPlayer.directionY}");
    //            }
    //        }
    //        //Debug.Log($"collided object: {collision.gameObject} \t direction: {collisionDirection}");
    //    }
    //}

    IEnumerator ApplyContinuousForce(Rigidbody2D rb, float forceAmount, Vector2 direction, float duration)
    {
        float timer = 0f;

        while (timer < duration)
        {
            rb.AddForce(forceAmount * Time.deltaTime * direction, ForceMode2D.Force);
            timer += Time.deltaTime;

            //if (CheckApproximity(rb.position.x, -0.3f) || CheckApproximity(rb.position.x, 5.3f) || CheckApproximity(rb.position.y, -0.3f)
            //|| CheckApproximity(rb.position.y, 9.3f) && canCheckOutOfBounds)
            {
            //    canCheckOutOfBounds = false;
                //Debug.Log("PlayerOutOfBounds");
              //  CheckRoundWinner();
               // break;
            }
            yield return null;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Tile") && !collision.gameObject.GetComponent<Tile>().isBroken)
        {
            iscolliding = false;
            //Debug.Log("Left Tile" + collision.name);
            //collision.GetComponent<Tile>().canBreak = false;
        }
        if (transform.position.x < 0 || transform.position.x > 5 || transform.position.y < 0 || transform.position.y > 9)
        {
            //Debug.Log($"Player out of bounds");
            isPlayerDead = true;
        }
    }
}

