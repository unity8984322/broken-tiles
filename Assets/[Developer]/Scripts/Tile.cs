using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
//using Random = UnityEngine.Random;

public class Tile : MonoBehaviour
{
    public int xPos;
    public int yPos;

    static Color defaultColor = Color.white;
    public ParticleSystem tileParticles;
    [SerializeField] Color lightBlueColor;
    [SerializeField] Color BlueColor;
    //public bool canBreak;
    public bool isBroken;
    public bool timerRunning = false;
    public float timeOnTile = 0f;
    //[SerializeField] float realTimeSinceStartup;
    [SerializeField] float timeThreshold = 4f;
    SpriteRenderer thisSprite;

    private void Start()
    {
        GetComponent<SpriteRenderer>().color = defaultColor;
        thisSprite = GetComponent<SpriteRenderer>();
        //thisSprite.sprite = TileGenerator.instance.tileSpriteLists[0];
    }

    private void CleanMess()
    {
        xPos = 0;
        yPos = 0;
        //canBreak = true;
        isBroken = false;
        timeOnTile = 0f;
        GetComponent<SpriteRenderer>().color = Color.white;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //if (isBroken && Player.instance.canBreakTile)
            //  return;
            //     canBreak = true;
            //Debug.Log($"Start Breaking: {collision.name}");
            if (!timerRunning && Player.instance.canBreakTile)
            {
                timerRunning = true;
                //Collider2D[] colliders = new Collider2D[2]; // Adjust the size of the array as per your requirement
                //int colliderCount = Physics2D.OverlapCollider(collision, new ContactFilter2D().NoFilter(), colliders);
                //Debug.Log(colliderCount + "///");
                StartCoroutine(BeginBreaking(collision.gameObject.GetComponent<Player>()));
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //      canBreak = false;
            //Debug.Log($"Stop Breaking: {collision.name}");
            timerRunning = false;
            StopCoroutine(nameof(BeginBreaking));
        }
    }

    IEnumerator BeginBreaking(Player playerWhoEntered)
    {
        if (playerWhoEntered.name == "Player - 2" && !TileGenerator.instance.vsComputer)
            timeThreshold += 0.2f;

        while (timerRunning && !TileGenerator.isGameComplete)
        {
            if (TileGenerator.roundComplete)
                yield break;

            timeOnTile += 0.1f;
            yield return new WaitForSeconds(0.1f);
            //Debug.Log("Timer: " + timer);
            //Debug.Log($"<color=Red> thisTile = {this.gameObject} \t playerWhoentered.CurrentTile = {playerWhoEntered.currentTile}</color>");

            //if (playerWhoEntered.currentTile != this.gameObject)
            //{
            //  canBreak = false;
            //yield break;
            //}

            //Debug.Log($"<color=Blue> Entered time = {enteredTime} </color>");
            //   while (this.canBreak)
            {
                //realTimeSinceStartup = Time.realtimeSinceStartup;

                //            enteredTime = Time.realtimeSinceStartup;
                //timeOnTile =  Time.realtimeSinceStartup - enteredTime;

                //yield return new WaitForSeconds(0.2f);
                //Debug.Log($"<color=Red> velocity = {playerWhoEntered.rb.velocity}</color>");

                if (timeOnTile < 1)
                {
                    
                    //if(playerWhoEntered.rb.velocity != Vector2.zero)
                    this.GetComponent<SpriteRenderer>().color = lightBlueColor;
                }
                else if (timeOnTile >= 1 && timeOnTile <= 1.3f)
                {
                    Debug.Log("Changed to lightblue");
                    if (MainMenu.DifficultyLevel == "Hard")
                    {
                        Debug.Log("<color=Green>changed direction of path on triggering tile</color>");

                        int randomValue = UnityEngine.Random.Range(0, 2) == 0 ? 1 : -1;
                        AIPlayer.directionX = randomValue;
                        randomValue = UnityEngine.Random.Range(0, 2) == 0 ? 1 : -1;
                        AIPlayer.directionY = randomValue;

                    }
                }
                else if (timeOnTile >= 1.8f && timeOnTile <= 2f)
                {
                    Debug.Log("Changed to blue");
                    //Debug.Log($"<color=Blue>TImeOnTile: {timeOnTile}</color>");

                        
                    //AIPlayer.instance.AIController();

                    this.GetComponent<SpriteRenderer>().color = BlueColor;
                    //   thisSprite.sprite = TileGenerator.instance.tileSpriteLists[1];
                }
                else if (timeOnTile >= 3f && timeOnTile <= 3.3f)
                {
                    //if (playerWhoEntered.rb.velocity != Vector2.zero)
                      //  tileParticles.Play();
                    Debug.Log($"<color=Red>TImeOnTile: {timeOnTile}</color>");
                    //AIPlayer.instance.AIController();

                    //this.GetComponent<SpriteRenderer>().DOBlendableColor(Camera.main.backgroundColor, 0.2f);
                    // thisSprite.sprite = TileGenerator.instance.tileSpriteLists[2];
                }
                if (timeOnTile >= timeThreshold)
                {
                    this.GetComponent<SpriteRenderer>().color = new Color(1,1,1,0);
                        TileGenerator.roundComplete = true;                    

                    playerWhoEntered.transform.DOScale(new Vector2(0.7f,0.7f), 0.3f).SetEase(Ease.InBounce).SetLoops(2, LoopType.Yoyo).OnComplete(() =>
                    {
                        ParticleSystem p = playerWhoEntered.GetComponent<ParticleSystem>();
                        p.Play();
                        playerWhoEntered.transform.DOScale(Vector3.zero, 03f).OnComplete(() => {
                            OnTimeThresholdReached(playerWhoEntered);
                            //StopCoroutine(nameof(BeginBreaking));
                            Debug.Log($"<color=White>TImeOnTile: {timeOnTile}</color>");
                            //playerWhoEntered.currentTile = null;
                        });                            
                    }
                    );                    
                }
            }
        }
    }

    private void OnTimeThresholdReached(Player p)
    {
        Debug.Log("Time threshold reached tile breaking=======");
        //reload round
        bool firstPScored;
        if (p.name == "Player - 1")
        {
            Debug.Log("Player 1 died");
            firstPScored = false;
            TileGenerator.instance.player2.score++;
            TileGenerator.instance.player1.isPlayerDead = true;
        }
        else
        {
            Debug.Log("Player 2 died");
            firstPScored = true;
            TileGenerator.instance.player1.score++;
            TileGenerator.instance.player2.isPlayerDead = true;
        }

        //Here game will be over as player is still inside tile and tile broke
        //  canBreak = false;
        isBroken = true;
        //timeOnTile = 0f; // Reset the timer
        timerRunning = false;
        p.canBreakTile = false;
        BreakTile();
        
        TileGenerator.instance.RoundComplete(firstPScored);
        //StopCoroutine(nameof(BeginBreaking));
        //Debug.Log("Time limit reached breaking the tile");

        // foreach (Tile t in TileGenerator.tilesArray)
        //      t.canBreak = false;

        if (p.score == 3)
            Debug.Log($"player {p} won with score = {p.score}");
    }

    public void SetPosition(int _x, int _y)
    {
        xPos = _x;
        yPos = _y;
    }

    public int CalculateManhattanDistance(Tile otherTile)
    {
        int distanceX = Mathf.Abs(xPos - otherTile.xPos);
        int distanceY = Mathf.Abs(yPos - otherTile.yPos);
        return distanceX + distanceY;
    }

    public bool IsClosestToLeftBorder(Tile leftmostTile, Tile rightmostTile)
    {
        int distanceToLeft = Mathf.Abs(xPos - leftmostTile.xPos - 1);
        int distanceToRight = Mathf.Abs(xPos - rightmostTile.xPos - 1);

        // Check if the distance to the left border is less than the distance to the right border
        return distanceToLeft < distanceToRight;
    }

    private void BreakTile()
    {
        //AIPlayer.instance.AIController();
        //TileGenerator.tilesArray[this.xPos, this.yPos] = null;
        //transform.DOPunchScale(new Vector3(1.01f, 1.2f, 0), 0.2f, 2, 1).OnComplete(() => transform.DOPunchScale(Vector3.zero, 0.2f, 2, 1));
        //.OnComplete(()=>Destroy(this.gameObject))
    }
}
