using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OuterColliders : MonoBehaviour
{

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Player p = collision.gameObject.GetComponent<Player>();
        p.canCheckOutOfBounds = false;
        Debug.Log("PlayerOutOfBounds");
        p.CheckRoundWinner();
    }
}
