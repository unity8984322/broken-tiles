using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class AIPlayer : MonoBehaviour
{
    public static AIPlayer instance;

    public Player player; // Reference to the Player script attached to this GameObject
    //private float maxDistance = 1f; // Number of nearby tiles to consider for movement
    //Tile nextTileToMove;    //not used
    [SerializeField] Tile nextTile;
    [SerializeField] Tile currentTile;
    //Queue<Tile> previousTile = new();

    //[SerializeField] Rigidbody2D square;
    public static float directionX;
    public static float directionY;
    public float moveSpeed = 100.0f;
    public Rigidbody2D rb;
    //private Vector2Int direction;
    public List<Tile> path = new();

    //static int count = 0;

    int boardHeight = 10;
    int boardWidth = 6;
    public static bool continueCoroutine = false;
    private void Start()
    {
        boardWidth = TileGenerator.instance.width;
        boardHeight = TileGenerator.instance.height;
        //if(instance = null)
        instance = this;
        //count = 0;
        player = GetComponent<Player>();

        //    direction = new Vector2Int(Random.Range(-1, 2), Random.Range(-1, 2));
        // Ensure initial direction is either horizontal or vertical
        //  if (direction.x != 0 && direction.y != 0)
        //{
        //    direction.x = 0;
        //}
        rb = transform.GetComponent<Rigidbody2D>();

        currentTile = player.currentTile;
    }

    private void OnEnable()
    {
        StartCoroutine(GenerateRandomPath());
        TileGenerator.roundCleanUp += CleanMess;
        continueCoroutine = true;
    }

    private void OnDisable()
    {
        TileGenerator.roundCleanUp -= CleanMess;
        //count = 0;
    }

    private void CleanMess()
    {
        nextTile = null;
        directionX = 0;
        directionY = 0;
        path.Clear();


        foreach (Tile t in TileGenerator.tilesArray)
            t.GetComponent<SpriteRenderer>().color = Color.white;

        Debug.Log("Starting AI path");

    }

    //[SerializeField] Tile[] nearbyTiles = new Tile[10];
    //[SerializeField] Tile tileToMove = null;
    public void AIController()
    {
        // Get the current position of the AI player
        Vector2 currentPosition = player.transform.position;

        // Find nearby tiles to consider for movement
        //nearbyTiles = FindNearbyTiles(currentPosition);
        //path.Add(player.currentTile);

        //if (path[^1] == player.currentTile) { }

        //  nearbyTiles = new Tile[path.Count];
        //foreach (Tile t in path)
        //{
        //Debug.Log($"{t.name} \t {t.xPos} : {t.yPos}");
        //  t.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0.5f);
        //}
        // Check if there are any nearby tiles
        if (path.Count > 0)
        {
            // Randomly select a tile to move to
            //tileToMove = nearbyTiles[Random.Range(0, nearbyTiles.Length)];
            Debug.Log("Start movement");
            // Move the AI player to the selected tile
            //StartCoroutine(MoveToTile());
        }
        else
        {
            // Handle the case when no nearby tiles are found
            Debug.LogWarning("No nearby tiles found for AI player.");
        }
    }

    //void Update()
    //{

    // Move the player
    //transform.Translate(new Vector2(direction.x,direction.y) * moveSpeed * Time.deltaTime);

    //// Check if the player has reached the border
    //if (transform.position.x <= 0 || transform.position.x >= 5 ||
    //    transform.position.y <= 0 || transform.position.y >= 9)
    //{
    //    Debug.Log("Direction Changed");
    //    // Change direction
    //    direction *= -1;
    //}
    //}

    private Vector2Int GetRandomDirection()
    {
        Vector2Int newDirection;

        do
        {
            int xDir = Random.Range(-1, 2);
            int yDir = Random.Range(-1, 2);

            newDirection = new Vector2Int(xDir, yDir);
        }
        while (newDirection == Vector2Int.zero);

        return newDirection;
    }


    private bool IsAtBorder()
    {
        return transform.position.x <= 0 || transform.position.x >= 5 ||
               transform.position.y <= 0 || transform.position.y >= 9;
    }


    //List<int> intList = new List<int> { 1, 3, 5, 7, 9, 8, 11 };
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //previousTile.Enqueue(collision.gameObject.GetComponent<Tile>());
        //if (collision.gameObject.CompareTag("Tile"))
        //Invoke(nameof(AIController), 1f);

        //if (collision.gameObject.CompareTag("Tile") && !IsAtBorder() && intList[Random.Range(0, intList.Count)] % 2 == 0)
        {
            //direction = GetRandomDirection();
        }
    }

    public IEnumerator MoveToTile()
    {
        yield return null;

        //if (player.currentTile == path[^1])
        {
            //Debug.Log("<color=violet>last tile reached</color>");
            //  StopCoroutine(MoveToTile());
            //yield break;
        }
        //for (int i = 0; i <= path.Count; i++)
        {
            //       nextTileToMove = null;
            //   if (path.Count > 1)
            //         nextTileToMove = path[path.IndexOf(player.currentTile) + 1];
            //Vector2.Lerp(transform.position, nextTileToMove.transform.position, 0.3f);

            //yield return new WaitForSeconds(01f);
            //transform.position =  Vector2.Lerp(transform.position, new Vector2(nextTileToMove.xPos, nextTileToMove.yPos), 0.4f);
            // transform.DOLocalMove(nextTileToMove.transform.position, 0.5f, false);
            //nextTileToMove.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0.5f);
            //Debug.Log($"moved to tile {nextTileToMove}");
            //yield return new WaitForSeconds(2f);
            // if (path[^3] != null && path.Count >2)
            {
                //    path.Remove(path[^2]);
                //   Debug.Log($"<color=Red> TileRemoved From path: : {path[^2]} </color>");
            }
        }
        //path = GenerateRandomPath();
        //foreach (Tile t in path)
        //{
        //Debug.Log($"{t.name} \t {t.xPos} : {t.yPos}");
        //t.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0.5f);
        //}
    }

    //private Tile[] FindNearbyTiles(Vector2 currentPosition)
    //{
    //    if (previousTile.Count >= 10)
    //    {
    //        for(int i=0;i<10;i++)
    //            previousTile.Dequeue();
    //    }
    //    // List to store nearby tiles
    //    List<Tile> nearbyTiles = new();

    //    // Iterate through all tiles to find nearby ones
    //    foreach (Tile tile in TileGenerator.tilesArray)
    //    {
    //        if (tile != null)
    //        {
    //            // Calculate distance between current position and tile position
    //            float distance = Vector2.Distance(currentPosition, tile.transform.position);

    //            // If the distance is within a certain range, add the tile to nearbyTiles list
    //            if (distance <= maxDistance && !nearbyTiles.Contains(tile) && !previousTile.Contains(tile))
    //            {
    //                nearbyTiles.Add(tile);
    //                Debug.Log($"Added tile = {tile.name}");
    //            }
    //        }
    //    }

    //    return nearbyTiles.ToArray();
    //}

    //public List<Tile> GenerateRandomPath(int minLength=5, int maxLength=7)
    //{
    //    List<Tile> path = new List<Tile>();

    //    // Start from a random tile on the grid
    //    Tile currentTile = TileGenerator.instance.GetTile((int)transform.position.x,(int)transform.position.y);
    //    Debug.Log($"<color=Red> Current Tile: {currentTile}</color>");
    //    if (!path.Contains(currentTile))path.Add(currentTile);

    //    // Generate a random path of length minLength to maxLength
    //    //int pathLength = Random.Range(minLength, maxLength);
    //    for (int i = 0; i < maxLength-1; i++)
    //    {
    //        // Get valid neighboring tiles
    //        Tile neighbor = null;
    //        //Tile neighbor = GetValidNeighbor(currentTile, 10);
    //        if (neighbor != null)
    //        {
    //            path.Add(neighbor);
    //            currentTile = neighbor;            
    //            neighbor.GetComponent<SpriteRenderer>().color = Color.green;
    //        }
    //        else
    //        {
    //            Debug.Log("No valid neighbour available");
    //            // If no valid neighbors, stop generating path
    //            break;
    //        }
    //    }

    //    return path;
    //}

    private Tile GetRandomTile(Tile[,] grid)
    {
        int randomX = Random.Range(0, grid.GetLength(0));
        int randomY = Random.Range(0, grid.GetLength(1));
        return grid[randomX, randomY];
    }

    //private Tile GetValidNeighbor(Tile tile, int maxLength)
    //{
    //    // Get the grid indices of the current tile
    //    int x = tile.xPos;
    //    int y = tile.yPos;

    //    // Define initial direction
    //    int dirX= -1 ;
    //    //int dirY = (y > 5) ? -1 : 1; // If yPos is above 5, move downward, else move upward

    //    int dirY = (y >= (Random.Range(5,9)) || y==9) ? -1 : 1; // If yPos is above 5, move downward, else move upward
    //    //int dirX = (x >= (Random.Range(3, 6)) || x==0) ? -1 : 1;
    //    if (x < Random.Range(0,3) || x== 0)
    //    {
    //         //Move right if xPos is below 3
    //        dirX = 1;
    //    }
    //    else if (x >= Random.Range(3,6) || x== 5)
    //    {
    //         //Move left if xPos is above 3
    //        dirX = -1;
    //    }
    //    else if(MainMenu.gameStarted)
    //    {
    //        // No initial horizontal movement if xPos is exactly 3
    //        dirX = -1;
    //    }

    //    for (int i = 1; i <= maxLength; i++) // Iterate up to maxLength
    //    {
    //        // Calculate the position of the neighbor
    //        int newX = x + dirX * i;
    //        int newY = y + dirY * i;

    //        // Check if the neighbor is within the grid
    //        if (IsWithinGrid(newX, newY))
    //        {
    //            return TileGenerator.tilesArray[newX, newY];
    //        }
    //        else
    //        {
    //            // Change direction if hitting grid boundary
    //            if (dirY == -1) // downward
    //            {
    //                // Try turning right if xPos is below 3
    //                // Try turning left if xPos is above 3
    //                dirX = (x < 3) ? 1 : (x > 3) ? -1 : 0;
    //                dirY = 0;
    //            }
    //            else // upward
    //            {
    //                // Try turning left if xPos is below 3
    //                // Try turning right if xPos is above 3
    //                dirX = (x < 3) ? -1 : (x > 3) ? 1 : 0;
    //                dirY = 0;
    //            }

    //            // Reset the position to try again with the new direction
    //            i = 0;
    //        }
    //    }

    //    return null; // Return null if no valid neighbor found
    //}


    [SerializeField] Vector2 movementDirection;
    private void Update()
    {
        if (instance == null || currentTile == null)
            return;

        //if (currentTile.yPos == 0 || currentTile.yPos == 9 || currentTile.xPos == 0 || currentTile.xPos == 5)
        //    if (MainMenu.DifficultyLevel == "Easy" && vsComputer)            
        //        if (Random.Range(1, 13) % 7 == 0)
        //        {
        //            Debug.Log("Throwing playe out of bounds");
        //            ThrowOutOfBound();                   
        //        }            

        //transform.position = moveSpeed * Time.deltaTime * movementDirection;
        //Debug.Log($"NextTilePosition: {nextTile.transform.position} \t transformPosition: {transform.position} \t moveDirection: {movementDirection}");


        //rb.velocity = movementDirection * moveSpeed * Time.deltaTime;
        //rb.velocity = moveSpeed * Time.deltaTime * movementDirection;
        //    rb.MovePosition(rb.position + moveSpeed * Time.deltaTime * movementDirection);
        //rb.velocity = moveSpeed * Time.deltaTime * movementDirection;
        if(nextTile != null && !TileGenerator.roundComplete)
        {
            //float horizontalInput = nextTile.transform.position.x;
            //float verticalInput = nextTile.transform.position.y;
            Vector2 direction = (nextTile.transform.position - transform.position).normalized;
            //rb.MovePosition(rb.position + moveSpeed * Time.deltaTime * direction);
            rb.velocity = moveSpeed * Time.deltaTime * direction;
            //Debug.Log($"NextTileLocalPosition: {nextTile.transform.localPosition} \t self local position: {transform.localPosition} \t NextTileWorld Position: {nextTile.transform.position} \t self world position : {transform.position}");
        }
    }

    private IEnumerator GenerateRandomPath(Tile startingTile = null)
    {

        yield return new WaitForSeconds(0.5f);
            yield return new WaitUntil(() => continueCoroutine);
        Debug.Log("<color=Black>000000000000000000000 IN GenerateRandomPath</color>");
        currentTile = player.currentTile;

        //currentTile = startingTile;
        path.Add(currentTile);

        if (currentTile == null)
            Debug.Log("CurrentTile = null");
        else
            Debug.Log("CurrentTile != null");
        directionY = currentTile.yPos >= boardHeight / 2 ? -1 : 1; // Start moving downwards or upwards
        directionX = currentTile.xPos >= boardWidth / 2 ? -1 : 1;

        if (TileGenerator.isGameComplete)
            yield break;

        while ((currentTile.yPos >= 0 && currentTile.yPos <= boardHeight - 1) && !TileGenerator.isGameComplete)
        {
            //below code to throw player out of bounds if at the edge in Easy mode
            if ((player.currentTile.yPos == 0 || player.currentTile.yPos == boardHeight - 1 || player.currentTile.xPos == 0 || player.currentTile.xPos == 5) && MainMenu.DifficultyLevel == "Easy")
            {
                Debug.Log($"Moving Along the border, looks good + currentTile.Xpos : {currentTile.xPos} \t currentTile.Ypos: {currentTile.yPos}");
                if (Random.Range(1, 11) % 3 == 0)
                {
                    Debug.Log("can throw");
                    continueCoroutine = false;
                    ThrowOutOfBound();
                }
            }

        Debug.Log($"Coroutine started: continueCoroutine: {continueCoroutine}");
            yield return new WaitUntil(() => continueCoroutine);
                //if (throwingOutOfBounds)
                  //  yield break;
            //if (MainMenu.gameStarted)
            //  directionX = 0;            

            int newY = (int)(currentTile.yPos + directionY);
            int newX = (int)(currentTile.xPos + directionX);

            Debug.Log($"<color=Red> DirectionX : {directionX} \t DirecitonY: {directionY} \t newX: {newX} \t newY: {newY}</color>");

            if (directionX == 0 && directionY == 0)
            {
                Debug.Log("<color=Yellow> Stay quite</color>");
                directionX = currentTile.xPos >= boardWidth / 2 ? -1 : 1;
                directionY = currentTile.yPos >= boardHeight / 2 ? -1 : 1;
            }

            if (currentTile.yPos == 0 || currentTile.yPos == boardHeight - 1 || currentTile.xPos == 0 || currentTile.xPos == 5)
            {

                Debug.Log("<color=blue> at any border</color>");
                // Check if player is at topmost or bottommost row                

                if (currentTile.yPos == 0)
                {
                    Debug.Log("<color=green> on the bottom border </color>");
                    if (directionY == -1)
                        directionY *= -1; // Reverse direction

                    //newY += 1;
                }
                else if (currentTile.yPos == boardHeight - 1)
                {
                    Debug.Log("<color=green> on the top ]border </color>");
                    if (directionY == 1)
                        directionY *= -1;

                    //newY -= 1;
                }


                newX = (newX == 0) ? newX + 1 : (newX == 9) ? newX - 1 : newX;
                // Adjust position if at the corners

                // Check if player is at the leftmost or rightmost column (excluding borders)

                if (currentTile.xPos == 0)
                {
                    Debug.Log("<color=brown> on the left border </color>");
                    if (directionX == -1)
                        directionX *= -1; // Reverse direction along X axis
                    //newX += 1;
                }
                else if (currentTile.xPos == boardWidth - 1)
                {
                    Debug.Log("<color=brown> on the right border </color>");
                    if (directionX == 1)
                        directionX *= -1; // Reverse direction along X axis
                    //newX -= 1;
                }

                if (directionX == 0)
                    directionX = currentTile.xPos == 6 ? -1 : 1;
            }
            else if (Random.Range(1, 5) % 2 == 0) // Randomly change direction every few steps
            {
                Debug.Log("<color=orange> in middle and randomly true of the board not on any border </color>");
                Debug.Log("Dont touch borders");
                // Randomly choose whether to move along X or Y axis
                bool moveAlongX = Random.Range(0, 2) % 2 == 0;

                //if (Random.Range(1, 15) % 7 == 0 && MainMenu.DifficultyLevel == "Easy")
                {
                    //GetBrokenTile();
                    //yield return new WaitForSeconds(0.7f);
                }

                if (moveAlongX)
                {
                    Debug.Log("<color=blue> in middle and randomly true so change direction of X </color>");
                    if (Random.Range(0, 2) % 2 == 0 && directionY != 0)
                        directionX *= -1;
                    else
                    {
                        //directionX = Random.Range(-1, 2);
                        //newY += Random.Range(-1, 2);
                        directionX = currentTile.xPos >= boardWidth / 2 ? -1 : 1;
                        //directionX *= -1; // Reverse direction along X axis
                    }
                }
                else
                {
                    Debug.Log("<color=red> in middle and randomly true so change direction of Y </color>");
                    if (Random.Range(0, 2) % 2 == 0 && directionX != 0)
                        directionY *= -1;
                    else
                    {
                        //directionY = Random.Range(-1, 2);
                        directionY = currentTile.xPos >= boardHeight / 2 ? -1 : 1;
                        //directionY *= -1; // Reverse direction along Y axis
                    }
                }
            }
            else        //if not at border and didin't run above else if
            {

                if (Random.Range(0, 2) % 2 == 0)
                {
                    Debug.Log("<color=green> IN middle so reverse direction of X </color>");
                    directionX *= -1;
                }
                else
                {
                    Debug.Log("<color=green> IN middle so change direction of Y </color>");
                    directionY *= -1;
                }
            }

            //currentTile.GetComponent<SpriteRenderer>().color = Color.white;
            if(Random.Range(0,10) %3 ==0 && MainMenu.DifficultyLevel == "Hard")
            {
                nextTile = GetBestTile();   
            }
            else
            {
                continueCoroutine = true; 
                nextTile = TileGenerator.instance.GetTile(newX, newY);
            }

            if (nextTile != null)
            {
                //yield return new WaitForSeconds(3f);

                path.Add(currentTile);
                //transform.DOMove(nextTile.transform.position, 0.7f, false).SetEase(Ease.Linear).OnComplete(() =>
                {
                    currentTile = nextTile;
                    Debug.Log($"<color=Green> moved to tile {currentTile}</color>");
                    //currentTile.GetComponent<SpriteRenderer>().color = Color.green;
                }//);

                yield return new WaitForSeconds(0.7f);
                //nextTile.GetComponent<SpriteRenderer>().color = Color.white;
            }
        }
        {
            //commented old while
            //{
            //while (currentTile.yPos >= 0 && currentTile.yPos <= boardHeight - 1) //continuously move in y direction
            //{
            //    if (MainMenu.gameStarted) directionX = 0;

            //    int newY = currentTile.yPos + directionY;
            //    int newX = currentTile.xPos + directionX;

            //    if (newY == 0 || newY == boardHeight-1) //|| newY == Random.Range(0,2) || newY== Random.Range(7,9)  //if player is at topmost or bottommost row
            //    {
            //        directionY *= -1; // Reverse direction

            //        if (newX == 0)      //if player is at the bottom left cornet
            //        {
            //            newX += 1;
            //        }
            //        else if (newX == 9)     //if player is at the bottom right cornet
            //        {
            //            newX -= 1;
            //        }
            //        else
            //        {
            //            Debug.Log("Except corners but at border");

            //            if (Random.Range(0, 4) % 2 == 0)        //moving in +X
            //            {
            //                Debug.Log($"Got random number %2==0 so keep moving in +X");
            //                newX += 1;
            //            }
            //            else        //calculate closest X border in move to opposite
            //            {
            //                Tile leftMostTile = TileGenerator.instance.GetLeftmostTile(newY);
            //                Tile rightMostTile = TileGenerator.instance.GetRightmostTile(newY);
            //                bool closestToLeftBorder = currentTile.IsClosestToLeftBorder(leftMostTile, rightMostTile);
            //                newX = (closestToLeftBorder) ? newX += 1 : newX -= 1;   
            //                Debug.Log($"<color=Blue>Got random number %2!=0 so checking which X border is nearest and moving opposite</color>" + newX);
            //            }
            //        }
            //        //Debug.Log($"newY: {newY} \t newX: {newX}");
            //    }

            //    //if (newY == 0 || newY == boardHeight - 1)
            //    {
            //    }
            //    //else if(newX == 0 || newX ==5)
            //    {
            //        //newY = newY <= boardHeight ? newY + 1 : newY - 1;
            //    }
            //    //if(newx)
            //    // Move horizontally
            //    //int newX = currentTile.xPos -1;
            //    //newX = Mathf.Clamp(newX, 0, boardWidth - 1);

            //    // Move vertically
            //    currentTile.GetComponent<SpriteRenderer>().color = Color.white;
            //    Tile nextTile = TileGenerator.instance.GetTile(newX, newY);
            //    // Get the tile at the new position
            //    Debug.Log($"<color=red>Tile to highlight and in the path: {nextTile} \t newX: {newX} \t newy: {newY}</color>");
            //    if (nextTile != null)
            //    {
            //        yield return new WaitForSeconds(0.5f);
            //        path.Add(currentTile);
            //        currentTile = nextTile;
            //        nextTile.GetComponent<SpriteRenderer>().color = Color.green;
            //        yield return new WaitForSeconds(0.5f);
            //        nextTile.GetComponent<SpriteRenderer>().color = Color.white;
            //    }

            //    // Change direction if reaching the top or bottom edge

        }
    }

    void ThrowOutOfBound()
    {
        Debug.Log("Throwing out of bounds");
        //Throuw out of bounds on random occassion on easy level
        if (currentTile.xPos == 0)
            transform.DOMoveX(-0.41f, 0.6f);
        else if (currentTile.xPos == 5)
            transform.DOMoveX(5.41f, 0.6f);
        else if (currentTile.yPos == 0)
            transform.DOMoveY(-0.41f, 0.6f);
        else if (currentTile.yPos == 9)
            transform.DOMoveY(9.41f, 0.6f);

        transform.DOScale(new Vector2(0.5f, 0.5f), 0.6f).OnComplete(() => { player.canCheckOutOfBounds = true;  });
    }

    Tile GetBestTile()
    {
        currentTile = player.currentTile;

        //call this function in easy mode sometimes
        Tile[] adjascentTiles = GetAdjacentTiles(currentTile.xPos, currentTile.yPos);
        var (minTimeOnTile, tileOnBreaking) = adjascentTiles.Where(tile => tile != null).Select(tile => (tile.timeOnTile, tile))
            .OrderBy(tuple => tuple.timeOnTile).FirstOrDefault();
        //withinBounds = true;     //this condition is just to avoid moving to different tile from GetRandomPath coroutine
        Debug.Log("TileoTobeBroken: " + tileOnBreaking);
        continueCoroutine = true;
        return tileOnBreaking;
        //transform.DOMove(tileOnBreaking.transform.position, 0.9f).OnComplete(() => { });
    }

    public Tile[] GetAdjacentTiles(int x, int y)
    {
        Tile[] adjacentTiles = new Tile[8]; // Array to store adjacent tiles

        // Define relative positions of adjacent tiles
        int[] dx = { -1, 0, 1, -1, 1, -1, 0, 1 };
        int[] dy = { -1, -1, -1, 0, 0, 1, 1, 1 };

        // Iterate through adjacent positions and check if they are within bounds
        for (int i = 0; i < 8; i++)
        {
            int nx = x + dx[i];
            int ny = y + dy[i];

            // Check if the adjacent position is within the grid bounds
            if (nx >= 0 && nx < boardWidth && ny >= 0 && ny < boardHeight)
            {
                adjacentTiles[i] = TileGenerator.tilesArray[nx, ny]; // Store the adjacent tile at the current index
                Debug.Log($"Adjascent Tile added : {TileGenerator.tilesArray[nx, ny]} \t timeOnTile: {adjacentTiles[i].timeOnTile}      centre: \t{x}\t{y}");
            }
            else
            {
                adjacentTiles[i] = null; // If the position is outside the grid bounds, store null
            }

        }

        return adjacentTiles;
    }

    public static bool IsWithinGrid(int x, int y)
    {

        return x >= 0 && x < TileGenerator.instance.width && y >= 0 && y < TileGenerator.instance.height && TileGenerator.tilesArray[x, y] != null;
    }
}