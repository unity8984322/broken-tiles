using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public static string DifficultyLevel="Easy";
    public static bool gameStarted = false;

    public void OnClickVsFriend()
    {
        gameStarted = true;
        UIManager.instance.ShowNextScreenAndHideAllOtherScreen(UIScreens_ENM.gamePlayScreen,false);
        TileGenerator.instance.counter.SetActive(true);
        TileGenerator.instance.blockImage.SetActive(false);
        TileGenerator.instance.SpawnTiles();

    }

    //public IEnumerator CounterTxt()
    //{
    //    TextMeshProUGUI text = TileGenerator.instance.counterText.GetComponentInChildren<TextMeshProUGUI>();
    //    TileGenerator.instance.OnClickPlay();

    //    yield return new WaitForSeconds(0.2f);
    //    text.text = "3";

    //    yield return new WaitForSeconds(1);
    //    text.text = "2";

    //    yield return new WaitForSeconds(1);
    //    text.text = "1";

    //    yield return new WaitForSeconds(1);

    //    TileGenerator.instance.counterText.SetActive(false);
    //}

    public void OnDrpoDownValueChanged(TextMeshProUGUI tmp)
    {
        DifficultyLevel = tmp.text;
        Debug.Log($"Selected Difficulty : {DifficultyLevel}");
    }

    public void OnClickPlayVsAI(TextMeshProUGUI tmp) 
    {        
        Time.timeScale = 1;
        if (DifficultyLevel != string.Empty && DifficultyLevel != "")
        {
            TileGenerator.instance.vsComputer = true;
            DifficultyLevel = tmp.text;
            Debug.Log($"Difficulty Level: {DifficultyLevel}");
            OnClickVsFriend();
        }

    }

    public void OnClickExitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
