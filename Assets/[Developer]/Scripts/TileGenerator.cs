using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using DG.Tweening;
public class TileGenerator : MonoBehaviour
{
    public static TileGenerator instance;

    public static Action roundCleanUp;

    public Player player1;
    public Player player2;
    [SerializeField] Tile tilePrefab; // Prefab of the tile
    [SerializeField] GameObject borderCollidervert;    //width and height are reversed
    [SerializeField] GameObject borderColliderhoriz;    //width and height are reversed
    public int width = 6; // Number of tiles horizontally
    public int height = 10; // Number of tiles vertically
    [SerializeField] float tileSize = 01f; // Size of each tile
    public GameObject counter;
    public GameObject blockImage;
    public bool vsComputer = false;
    public bool hasPlayer1Won = false;
    public static bool roundComplete;
    public static bool isGameComplete = false;
    [SerializeField] TextMeshProUGUI redScoreTxt;
    [SerializeField] TextMeshProUGUI blueScoreTxt;

    //public List<Sprite> tileSpriteLists;
    public static Tile[,] tilesArray;


    public void OnClickPlay()
    {
        tilesArray = new Tile[6, 10];
    }

    void Start()
    {
        vsComputer = false;

        if (instance != this)
            instance = this;

    }

    public void SpawnTiles()
    {
        StartCoroutine(nameof(GenerateTiles));
    }


    Vector3 startingPosition;
    public IEnumerator GenerateTiles()
    {
        tilesArray = new Tile[6, 10];
        tilesArray.Initialize();
        //startingPosition = new Vector3((-width)* tileSize / 2, (-height)* tileSize / 2f, 0);
        Vector3 cameraPosition = new Vector3((width) * tileSize, (height) * tileSize, -10) + new Vector3(-0.5f, 0f, 0);
        Camera.main.transform.position = cameraPosition;

        for (int y = 0; y < height; y++)
        {
            int i = 0;
            for (int x = 0; x < width; x++)
            {
                int j = 0;
                Vector3 tilePosition = startingPosition + new Vector3(x * tileSize * 2, y * tileSize * 2, 0);
                Tile tileCone = Instantiate(tilePrefab, tilePosition, Quaternion.identity, this.transform.GetChild(0));
                tileCone.name += " - " + x.ToString() + " - " + y.ToString();
                tilesArray[x, y] = tileCone;
                tileCone.GetComponent<Tile>().SetPosition(x, y);
                j++;
                i++;
            }
            yield return null;
        }

        player1.score = 0;
        player2.score = 0;
        redScoreTxt.text = "0";
        blueScoreTxt.text = "0";
        GenerateBorderColliders();
    }

    public void SetPlayers()
    {
        foreach (Tile t in tilesArray)
        {
            t.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            t.isBroken = false;
            t.timeOnTile = 0;
            t.timerRunning = false;
        }

        Debug.Log("This one is activating players?");
        player1.gameObject.SetActive(true);
        player2.gameObject.SetActive(true);
        player1.currentTile = tilesArray[4, 1];
        player2.currentTile = tilesArray[1, 8];
        player1.isPlayerDead = false;
        player2.isPlayerDead = false;
        player1.transform.DOScale(new Vector2(0.5f, 0.5f), 0.3f);
        player2.transform.DOScale(new Vector2(0.5f, 0.5f), 0.3f);
        player1.transform.position = tilesArray[4, 1].transform.position;
        player2.transform.position = tilesArray[1, 8].transform.position;
        player1.fixedJoystick.gameObject.SetActive(true);
        //player2.fixedJoystick.gameObject.SetActive(true);
        Time.timeScale = 1;

        Invoke(nameof(EnablePlayerColliders), 0.5f);
        //AIPlayer.instance.AIController();
    }

    void EnablePlayerColliders()
    {
        player2.GetComponent<CircleCollider2D>().enabled = true;
        player1.GetComponent<CircleCollider2D>().enabled = true;
    }

    public Tile GetTile(int x, int y)
    {
        if (x >= 0 && x < width && y >= 0 && y < height)
        {
            //Debug.Log($"---X:{x} Y: {y}---");
            return tilesArray[x, y];
        }
        else
        {
            Debug.LogWarning("Invalid tile coordinates");
            return null;
        }
    }

    public Tile GetLeftmostTile(int row)
    {
        //Debug.Log("row passed : " + row);
        Tile leftmostTile = tilesArray[0, row]; // Since xPos starts from 0, the leftmost tile will be at index 0
        return leftmostTile;
    }

    // Method to get the rightmost tile for a given row
    public Tile GetRightmostTile(int row)
    {
        //Debug.Log("row passed : " + row);
        Tile rightmostTile = tilesArray[5, row]; // GetLength(1) gives the length of the second dimension (columns)
        return rightmostTile;
    }

    public void RoundComplete(bool AIplayerOut)
    {
        //if (player2.isPlayerDead && player1.isPlayerDead)
        {
            //  Debug.Log("both players were dead");

        }

        //roundComplete = true;
        foreach (Tile t in tilesArray)
        {
            t.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            t.isBroken = false;
            t.timeOnTile = 0;
            t.timerRunning = false;
        }

        if (AIplayerOut)
        {
            redScoreTxt.text = player1.score.ToString();
            //DOTween.Sequence().Append(player2.transform.DOScale(Vector2.zero, 0.4f)).Append(player2.transform.DOScale(new Vector2(0.5f, 0.5f), 0.4f));
        }
        else
        {
            blueScoreTxt.text = player2.score.ToString();
            //DOTween.Sequence().Append(player1.transform.DOScale(Vector2.zero, 0.4f)).Append(player1.transform.DOScale(new Vector2(0.5f,0.5f), 0.4f));
        }


        player1.isPlayerDead = true;
        player2.isPlayerDead = true;

        //transform.DOMove(transform.position,    1.3f).OnComplete(() => 
        {

            if (player1.score == 3 || player2.score == 3)
            {
                player2.GetComponent<AIPlayer>().enabled = false;

                hasPlayer1Won = player1.score == 3;
                UIManager.instance.ShowScreen(UIScreens_ENM.resultScreen, false);
                return;
            }

            roundCleanUp.Invoke();
            ChangeActiveState();

        }//);

        //player2.transform.DOScale(Vector3.zero, 0.1f);
        //player1.transform.DOScale(Vector3.zero, 0.1f).OnComplete(() =>
        {
            //Debug.Log("running?");
        }//);

        //if (player1.score >= 3 || player2.score >=3)
        {
            //  UIManager.instance.ShowScreen(UIScreens_ENM.resultScreen,false);
        }
        //transform.DOScale(Vector3.one,6).OnComplete(() => ChangeActiveState());
        //counterText.SetActive(true);
        //StartCoroutine(nameof(MainMenu.CounterTxt));
    }
    public void ChangeActiveState()
    {
        foreach (Tile t in tilesArray)
        {
            t.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            t.isBroken = false;
            t.timeOnTile = 0;
            t.timerRunning = false;
        }

        AIPlayer.continueCoroutine = true;
        Time.timeScale = 1;
        player1.gameObject.SetActive(!player1.gameObject.activeSelf);
        player2.gameObject.SetActive(!player2.gameObject.activeSelf);
        Debug.Log($"Player's Active state changed to p1:{player1.gameObject.activeSelf}\t p2{player2.gameObject.activeSelf}");

        //Invoke(nameof(SetPlayers), 3);
        if (isGameComplete)
            return;

        counter.SetActive(true);
    }

    void GenerateBorderColliders()
    {
        // Calculate the total size of the board
        float boardWidth = width * tileSize;
        float boardHeight = height * tileSize;

        // Instantiate colliders for each border
        InstantiateCollider(borderCollidervert,-boardWidth +1.9f, 4.5f ); // Left border
        InstantiateCollider(borderCollidervert,boardWidth + 3.1f, 4.5f ); // Right border
        InstantiateCollider(borderColliderhoriz,2.5f, boardHeight + 5.1f); // Top border
        InstantiateCollider(borderColliderhoriz,2.5f, -boardHeight + 3.9f); // Bottom border
    }

    // Instantiate a border collider with given parameters
    void InstantiateCollider(GameObject prefab,float offsetX, float offsetY)
    {
        Vector3 position = transform.position + new Vector3(offsetX, offsetY, 0);
        GameObject bc = GameObject.Instantiate(prefab, position,prefab.transform.rotation,transform);
        //borderCollider.transform.localScale = new Vector3(width, height, 1);
    }

    public void OnClickMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void OnClickExitGame()
    {
        Application.Quit();
    }
}
