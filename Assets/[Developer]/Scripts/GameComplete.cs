using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class GameComplete : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI blueWinTxt;
    [SerializeField] TextMeshProUGUI redWinTxt;
    [SerializeField] Color redWinColor;
    [SerializeField] Color blueWinColor;
    [SerializeField] Image bgImage;

    private void OnEnable()
    {
        TileGenerator.isGameComplete= true;
        //Time.timeScale = 0;
        blueWinTxt.transform.DOScale(0, 0);        
        redWinTxt.transform.DOScale(0, 0);        

        if (!TileGenerator.instance.hasPlayer1Won)
        {
            blueWinTxt.gameObject.SetActive(true);
            blueWinTxt.transform.DOScale(Vector3.one, 0.2f);
            bgImage.color = blueWinColor;
        }
        else
        {
            redWinTxt.gameObject.SetActive(true);
            redWinTxt.transform.DOScale(Vector3.one, 0.2f);
            bgImage.color = redWinColor;
        }


    }

    private void OnDisable()
    {
        TileGenerator.isGameComplete = false;
    }

    

}
