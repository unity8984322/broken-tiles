using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class CounterText : MonoBehaviour
{

    public void OnEnable()
    {
        StartCoroutine(CounterTxt());      
    }

    public IEnumerator CounterTxt()
    {
        TextMeshProUGUI text = TileGenerator.instance.counter.GetComponentInChildren<TextMeshProUGUI>();
        //TileGenerator.instance.OnClickPlay();

        yield return new WaitForSeconds(0.2f);
        text.text = "3";

        yield return new WaitForSeconds(1);
        text.text = "2";

        yield return new WaitForSeconds(1);
        text.text = "1";

        yield return new WaitForSeconds(1);

        text.text = "";
        TileGenerator.instance.counter.SetActive(false);
        TileGenerator.instance.SetPlayers();

        foreach (Tile t in TileGenerator.tilesArray)
        {
            t.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            t.isBroken = false;
            t.timeOnTile = 0;
            t.timerRunning = false;
        }

        TileGenerator.roundComplete = false;
    }
}
